#Comandos
## Añadir proyecto a Bitbucket
Guía sencilla para comenzar con git.

Para crear un nuevo repositorio de git:
``` shell
git init
```

> Agregar a .gitignore .idea para ignorar la carpeta

Registrar cambios (añadirlos al Index):
```shell
git add .
```

Mirar los cambios realizados:
```shell
git status -s
```

Hacer commit de los últimos cambios:
```shell
git commit -m "message"
```

Conectar repositorio local a un repositorio remoto:
```shell
git remote add origin <url>
```

Enviar cambios a repositorio remoto(por primera vez):
```shell
git push -u origin --all --force
```
